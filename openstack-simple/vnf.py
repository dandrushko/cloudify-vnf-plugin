########
# Copyright (c) 2017 Mirantis Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
#    * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    * See the License for the specific language governing permissions and
#    * limitations under the License.


from cloudify import ctx
from cloudify.state import ctx_parameters
from cloudify.decorators import operation
from cloudify.exceptions import NonRecoverableError

from keystoneauth1.identity import v3
from keystoneauth1 import session
from novaclient import client as nova

# OpenStack instance state
VNF_ACTIVE_STATE = 'ACTIVE'
VNF_BUILD_STATE = 'BUILD'


@operation
def create(**kwargs):
    # Printing out ctx object for debuging purpose
    #for item, value in ctx.iteritems():
    ctx = kwargs['ctx']
    ctx.logger.debug("Context object: {0}".format(ctx))
    # We won't install into VNF, so, configure this property to False
    #ctx.node.properties['install_agent'] = False

    # Getting auth dict from the deployment
    auth_dict = ctx.node.properties['openstack_config']
    # Creating v3 Authentication session with Openstack
    os_session = _get_keystone_creds(auth_dict)
    nova_connection = nova.Client("2", session=os_session)

    #net_name = ctx.node.properties['management_network_name']
    net = nova_connection.networks.find(label=ctx.node.properties['management_network_name'])
    ctx.logger.debug(
        "Network was {0} was requested from the OpenStack, result is : {1}".format(ctx.node.properties['management_network_name'], net))

    vnf_image = nova_connection.images.find(name=ctx.node.properties['image'])
    vnf_flavor = nova_connection.flavors.find(name=ctx.node.properties['flavor'])

    # Launching VNF
    vnf_instance = nova_connection.servers.create(name=ctx.node.properties['vnf_name'],
                            image=vnf_image.id,
                            flavor=vnf_flavor.id,
                            nics=[{'net-id': net.id}])

    # Adding vnf_id into the runtime properties
    ctx.instance.runtime_properties['vnf_id'] = vnf_instance.id
    ctx.logger.debug(
        "VNF was launched. Instance details: {0}".format(vnf_instance))


@operation
def start(**kwargs):
    """
    VNF creation might take a long time, so let's retry until it change state to ACTIVE
    :param kwargs: contains context object with all run time parameters
    :return:
    """
    ctx = kwargs['ctx']

    # Get VNF details from the context object
    vnf_id = ctx.instance.runtime_properties['vnf_id']

    #Due to stupid implementation should authentication and recreate nova_client object
    # Getting auth dict from the deployment
    auth_dict = ctx.node.properties['openstack_config']
    # Creating v3 Authentication session with Openstack
    os_session = _get_keystone_creds(auth_dict)
    nova_connection = nova.Client("2", session=os_session)

    # Getting instance id
    openstack_instance = nova_connection.servers.get(vnf_id)

    ctx.logger.debug("VNF state is: {0}".format(openstack_instance))

    if openstack_instance.status not in [VNF_ACTIVE_STATE, VNF_BUILD_STATE]:
        ctx.logger.debug("VNF creation failed, VNF {0} has state {1}".format(openstack_instance.id, openstack_instance.status))
        raise NonRecoverableError("VNF creation failed, VNF {0} has state {1}".format(openstack_instance.id, openstack_instance.status))

    if openstack_instance.status == VNF_BUILD_STATE:
        return ctx.operation.retry(message='Waiting for VNF to come into the \'Active\' state',
                    retry_after=kwargs['start_retry_interval'])

    # Saving Management IP of the VNF instance
    #management_network = ctx.node.properties['management_network_name']
    management_ip = openstack_instance.networks.items()
    for net, ip in openstack_instance.networks.items():
        if net == ctx.node.properties['management_network_name']:
            ctx.instance.runtime_properties['ip'] = ip[0]
    return


@operation
def stop(**kwargs):
    # No much value right now to has special implementation here
    pass


@operation
def delete(**kwargs):

    ctx = kwargs['ctx']
    # Get VNF details from the context object
    vnf_id = ctx.instance.runtime_properties['vnf_id']

    # Due to stupid implementation we should authenticate and recreate nova_client object
    # Getting auth dict from the deployment
    auth_dict = ctx.node.properties['openstack_config']
    # Creating v3 Authentication session with Openstack
    os_session = _get_keystone_creds(auth_dict)
    nova_connection = nova.Client("2", session=os_session)

    #TODO: We should handle delete reply from the Openstack
    nova_connection.servers.delete(vnf_id)
    return




def _get_keystone_creds(auth_data):
    """
    We should provide dict with auth details:
    :param             {auth_url=URL,
                       username=USERNAME,
                       password=PASSWORD,
                       project_name=PROJECT_NAME,
                       user_domain_id='default',
                       project_domain_id='default'}
    derived from the TOSCA blueprint
        username: { get_input: keystone_username }
        password: { get_input: keystone_password }
        project_name: { get_input: keystone_tenant_name }
        auth_url: { get_input: keystone_url }
        user_domain_id: 'default'
        project_domain_id: 'default'
    :return:
    """
    v3_auth = v3.Password(auth_url=auth_data['auth_url'],
                          username=auth_data['username'],
                          password=auth_data['password'],
                          project_name=auth_data['project_name'],
                          user_domain_id=auth_data['user_domain_id'],
                          project_domain_id=auth_data['project_domain_id'])
    sess = session.Session(auth=v3_auth)
    #sess._http_log_request = custom_request(sess._http_log_request)
    return sess