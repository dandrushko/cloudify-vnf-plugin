########
# Copyright (c) 2014 GigaSpaces Technologies Ltd. All rights reserved
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
#    * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    * See the License for the specific language governing permissions and
#    * limitations under the License.


from setuptools import setup

# Replace the place holders with values for your project

setup(

    # Do not use underscores in the openstack-simple name.
    name='openstack-simple-openstack-simple',

    version='0.1',
    author='Dmitriy Andrushko',
    author_email='dandrushko [at] mirantis . com',
    description='Basic openstack-simple which spawns VNF for basic NFV demoes and PoCs',

    # This must correspond to the actual packages in the openstack-simple.
    packages=['openstack-simple'],

    license='Mirantis Inc. All rights reserved',
    zip_safe=False,
    install_requires=[
        'cloudify-plugins-common>=3.3.1',
        'keystoneauth1>=2.16.0,<3',
        'python-novaclient==7.1.0'
    ],
    test_requires=[
         "cloudify-dsl-parser>=3.4.1"
        "nose"
    ]
)
